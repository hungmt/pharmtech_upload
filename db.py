'''
Created on Sep 29, 2019

@author: hungmt
'''

from datetime import datetime

sql_insert_ticket_query = """ INSERT INTO ticket_attachments (ticket_id, attachment_type, path, created_date) 
                            VALUES (%s,%s,%s,%s)"""

sql_insert_diagnose_query = """ INSERT INTO attachments (diagnose_id, type, path) 
                            VALUES (%s,%s,%s)"""

def insert_ticket(db_connection, ticket_id, attachment_type, path, created_date):
    print('Begin insert_ticket with [' + 'ticket_id=' + ticket_id + ',attachment_type=' + str(attachment_type) + ',path=' + path + ',created_date=' + str(created_date) + ']')
    db_cursor = db_connection.cursor(prepared=True)
    
    if created_date is None:
        now = datetime.now()
        created_date = now.strftime('%Y-%m-%d %H:%M:%S')
        
    record = (ticket_id, attachment_type, path, created_date)
    db_cursor.execute(sql_insert_ticket_query, record)
    db_connection.commit()
    print('End insert_ticket')
    
def insert_diagnose(db_connection, diagnose_id, attachment_type, path):
    print('Begin insert_diagnose with [' + 'diagnose_id=' + diagnose_id + ',attachment_type=' + str(attachment_type) + ',path=' + path + ']')
    db_cursor = db_connection.cursor(prepared=True)
    record = (diagnose_id, attachment_type, path)
    db_cursor.execute(sql_insert_diagnose_query, record)
    db_connection.commit()
    print('End insert_diagnose')
    