'''
Created on Sep 28, 2019

@author: hungmt
'''
import json
import config
import resize
import db
from datetime import datetime
import mysql.connector

TICKET_TYPE = 'ticket'
DIAGNOSE_TYPE = 'diagnose'

ticket_db_connection = mysql.connector.connect(host=config.ticket_db_host, user=config.ticket_db_user, passwd=config.ticket_db_passwd, database=config.ticket_db_database)
print('ticket_db_host=' + config.ticket_db_host + ',ticket_db_user=' + config.ticket_db_user + ',ticket_db_passwd=' + config.ticket_db_passwd + ',ticket_db_database=' + config.ticket_db_database)

diagnose_db_connection = mysql.connector.connect(host=config.diagnose_db_host, user=config.diagnose_db_user, passwd=config.diagnose_db_passwd, database=config.diagnose_db_database)
print('diagnose_db_host=' + config.diagnose_db_host + ',diagnose_db_user=' + config.diagnose_db_user + ',diagnose_db_passwd=' + config.diagnose_db_passwd + ',diagnose_db_database=' + config.diagnose_db_database)


def build_s3_full_output_path(bucket_name, s3_output_path):
    return bucket_name + '/' + s3_output_path

def get_info_from(s3_path):
    return tuple(s3_path.split('/'))

def get_upload_type(s3_path):
    input_type = s3_path.split('/')[2]
    return  input_type[0:input_type.rfind('_input')]
    
def build_s3_output_path(tennant_id, branch_id, upload_output, user_id, patient_id, type_id, file_name):
    return  tennant_id + '/' + branch_id + '/' + upload_output + '/' + user_id + '/' + patient_id + '/' + type_id + '/' + file_name;

def get_file_extension(s3_input_path):
    index = s3_input_path.rfind('.')
    return s3_input_path[index:]

def get_file_name(ext):
    yyyyMMddhhmm = datetime.now().strftime("%Y%m%d%H%M")
    return yyyyMMddhhmm[2:] + ext

def get_created_date():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S');

def lambda_handler(event, context):
    global ticket_db_connection
    global diagnose_db_connection
    
    input_bucket_name = config.input_bucket_name
    output_bucket_name = config.output_bucket_name
    
    for sqs_record in event['Records']: 
        sqs_body = sqs_record['body']
        print('sqs_body:' + sqs_body)
        
        s3_event = json.loads(sqs_body)
        
        for s3_record in s3_event['Records']:
            print('s3_record:' + str(s3_record))
            # input path has format '1/2/ticket_input/11/9999/thongvv3.jpeg'
            s3_input_path = s3_record['s3']['object']['key']
            print('s3_input_path:' + s3_input_path)

            # bucket name
            trigger_bucket_name = s3_record['s3']['bucket']['name']
            print('trigger_bucket_name:' + trigger_bucket_name)
            
            if trigger_bucket_name != input_bucket_name:
                return
            
            # type of upload: ticket or diagnose, parse from input path
            upload_type = get_upload_type(s3_input_path)
            print('upload_type:' + upload_type)
            
            ext = get_file_extension(s3_input_path)
            file_name = get_file_name(ext)
    
            if upload_type == TICKET_TYPE:
                upload_output = upload_type + '_output'
                print('upload_output:' + upload_output)
                
                tennant_id, branch_id, _, user_id, patient_id, ticket_id, _ = get_info_from(s3_input_path)
                
                # Create output path
                s3_output_path = build_s3_output_path(tennant_id, branch_id, upload_output, user_id, patient_id, ticket_id, file_name)
                print('s3_output_path:' + s3_output_path)
                
                # Resize image and put it into same bucket
                resize.resize(input_bucket_name, output_bucket_name, s3_input_path, s3_output_path)
                
                # Insert into database
                attachment_type = 1; # hardcode attachment type for image type
                created_date = get_created_date()
                
                # Build full output path for resized image
                s3_full_output_path = build_s3_full_output_path(output_bucket_name, s3_output_path)
                print('s3_full_output_path:' + s3_full_output_path)
                
                # Insert values to DB
                db.insert_ticket(ticket_db_connection, ticket_id, attachment_type, s3_full_output_path, created_date)
                
                print('Success!')
            
                
            if upload_type == DIAGNOSE_TYPE:
                upload_output = upload_type + '_output'
                print('upload_output:' + upload_output)
                
                tennant_id, branch_id, _, user_id, patient_id, diagnose_id, _ = get_info_from(s3_input_path)
                
                # Create output path
                s3_output_path = build_s3_output_path(tennant_id, branch_id, upload_output, user_id, patient_id, diagnose_id, file_name)
                print('s3_output_path:' + s3_output_path)
                
                # Resize image and put it into same bucket
                resize.resize(input_bucket_name, output_bucket_name, s3_input_path, s3_output_path)
                
                # Insert into database
                attachment_type = 1; # hardcode attachment type for image type
                 
                # Build full output path for resized image
                s3_full_output_path = build_s3_full_output_path(output_bucket_name, s3_output_path)
                print('s3_full_output_path:' + s3_full_output_path)
                
                # Insert values to DB
                db.insert_diagnose(diagnose_db_connection, ticket_id, attachment_type, s3_full_output_path)
                
                print('Success!')
                