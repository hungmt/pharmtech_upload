'''
Created on Sep 29, 2019

@author: hungmt
'''

from __future__ import division
import boto3
from PIL import Image
import tempfile

SIZE = 1280

s3client = boto3.client('s3')

def get_new_dimension(width, height):
    if width > height:
        new_heigh = int(SIZE * height / width)
        return (SIZE, new_heigh)
    else:
        new_width = int(SIZE * width / height)
        return (new_width, SIZE)

def create_resized_image(image):
    print('Begin create_resized_image')
    width, height = image.size
    new_width, new_height = get_new_dimension(width, height)
    resized_image = image.resize((new_width,new_height), Image.ANTIALIAS)
    tmp_file = tempfile.NamedTemporaryFile(suffix='.png')
    
    resized_image.save(tmp_file.name)
    print('End create_resized_image')
    return tmp_file;

def get_image_from_s3_to_local(bucket_name, s3_path):
    print('Begin get_image_from_s3_to_local with [' + 'bucket_name=' + bucket_name + ',s3_path=' + s3_path + ']')
    tmp_file = tempfile.NamedTemporaryFile(suffix='.png')
    with open(tmp_file.name, 'wb') as f:
        s3client.download_fileobj(bucket_name, s3_path, f)
    
    print('End get_image_from_s3_to_local')
    return tmp_file;

def upload_from_local_to_s3(local_image_name, bucket_name, s3_path):
    print('Begin upload_from_local_to_s3 with [' + 'bucket_name=' + bucket_name + ',s3_path=' + s3_path + ']')
    s3client.upload_file(local_image_name.name, bucket_name, s3_path)
    print('End upload_from_local_to_s3')
    
def resize(input_bucket_name, output_bucket_name, input_s3_path, output_s3_path):
    print('Begin resize')
    local_input_file = get_image_from_s3_to_local(input_bucket_name, input_s3_path);
    image = Image.open(local_input_file.name)
    width, height = image.size
    if max(width, height) < SIZE:
        upload_from_local_to_s3(local_input_file, output_bucket_name, output_s3_path);
        print('End resize. Do not resize image')
        return False;
    else:
        local_resized_file = create_resized_image(image);
        upload_from_local_to_s3(local_resized_file, output_bucket_name, output_s3_path);
        print('End resize. Resize successfully!')
        return True;
    